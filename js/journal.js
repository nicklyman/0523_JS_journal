var moment = require('moment');

exports.Journal = function(title, body) {
  this.title = title;
  this.body = body;
  this.currentDate = moment().format('LLLL');
};

exports.Journal.prototype.entry = function() {
  this.journalEntry = "Date: " + this.currentDate + " Title: " + this.title + ", here is what happened today: " + this.body;
  return this.journalEntry;
};

exports.Journal.prototype.countWords = function() {
  var arrayWords = this.journalEntry.split(" ");
  var numberOfWords = 0;
  arrayWords.forEach(function(word) {
    numberOfWords += 1;
  });
  return numberOfWords;
};
