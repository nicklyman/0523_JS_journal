var Journal = require('.././js/journal.js').Journal;

$(document).ready(function() {
  $('#journal_entry').submit(function(event){
    event.preventDefault();
    var journalTitle = $('#title').val();
    var journalBody = $('#body').val();
    var newJournal = new Journal(journalTitle, journalBody);
    $("#entries").append("<li>" + newJournal.entry() + " this entry has " + newJournal.countWords() + " words </li>");
  });
});
